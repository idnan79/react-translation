import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Login from "./components/Login/Login";
// import NotFound from "./components/NotFound/NotFound";
import AppContainer from "./hoc/AppContainer";
import Translatepage from "./components/TranslatePage/Translatepage";
import Register from "./components/Register/Register";

function App() {
  return (
    <BrowserRouter>
      <div className="App p-3 mb-2 bg-warning text-dark">
        <AppContainer>
          <h1>Lost in Translation</h1>
        </AppContainer>
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="*" component={Translatepage} />
          <Route path="/register" exact component={Register} />
          {/* //<Route path="*" component={NotFound} /> */}
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
