import { composeWithDevTools } from "redux-devtools-extension";
import appReducer from "./reducers";
import middleware from "./middleware";
import { createStore } from "redux";

export default createStore(appReducer, composeWithDevTools(middleware));
