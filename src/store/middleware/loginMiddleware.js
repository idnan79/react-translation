import { LoginAPI } from "../../components/Login/LoginAPI";
import {
  ACTION_LOGIN_ATTEMPTING,
  ACTION_LOGIN_SUCCESS,
  loginSuccessAction,
  loginErrorAction,
} from "../actions/loginActions";
import { sessionSetAction } from "../actions/sessionActions";

export const loginMiddleware = ({ dispatch }) => (next) => (action) => {
  next(action);

  if (action.type === ACTION_LOGIN_ATTEMPTING) {
    //make a ttp request to access
    LoginAPI.login(action.payload)
      .then((profile) => {
        dispatch(loginSuccessAction(profile)); //loginsuccess
      })
      .catch((error) => {
        dispatch(loginErrorAction(error.message));
      });
  }
  if (action.type === ACTION_LOGIN_SUCCESS) {
    dispatch(sessionSetAction(action.payload));
  }
};
