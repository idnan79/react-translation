function imageStorage() {
  function imageImport(i) {
    let images = {};
    i.keys().map((item, index) => {
      images[item.replace("./", "")] = i(item);
    });
    return images;
  }

  const images = imageImport(
    require.context("./../individualSign", false, /\.png/)
  );
  return images;
}

export default imageStorage;
