import imageStorage from "../../imageStorage/imageImport";

function ProfileView(props) {
  const imageSigns = imageStorage();

  const alphabetArray = props.word.split("");

  return (
    <div>
      <div>
        <p>{props.word}</p>
      </div>
      <div>
        <div>
          {alphabetArray.map(
            (alphabet) =>
              imageSigns[`${alphabet}.png`] && (
                <img
                  width="100px"
                  src={imageSigns[`${alphabet}.png`].default}
                  alt="asd"
                />
              )
          )}
        </div>
      </div>
    </div>
  );
}

export default ProfileView;
