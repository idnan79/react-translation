import imageStorage from "../../imageStorage/imageImport";

function TranslationView(props) {
  const images = imageStorage();
  const alphabetArray = props.wordToTranslate.split("");

  return (
    <div>
      <div>
        <div>
          {alphabetArray.map(
            (alphabet) =>
              images[`${alphabet}.png`] && (
                <img
                  width="100px"
                  src={images[`${alphabet}.png`].default}
                  alt="asd"
                />
              )
          )}
        </div>
      </div>
    </div>
  );
}

export default TranslationView;
