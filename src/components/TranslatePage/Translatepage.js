import { useHistory } from "react-router-dom";
import { useState } from "react";
import imageArray from "../../imageStorage/imageImport";
// import imageArray from "../../";
import TranslationView from "../TranslatePage/TranslatePageAPI";
import AppContainer from "../../hoc/AppContainer";

const Translation = () => {
  const history = useHistory();

  const [translate, setTranslate] = useState("");

  let [wordToTranslate, setWordToTranslate] = useState("");

  const [translatedWords, setTranslatedWords] = useState([]);

  //retrieves name from session storage.
  const getData = () => {
    let data = sessionStorage.getItem("name");
    //let data = JSON.parse(sessionStorage.getItem("rtxtf-ss"))
    return data;
  };

  const onTranslate = (event) => {
    setTranslate(event.target.value);
  };

  const handleTranslate = () => {
    setWordToTranslate((wordToTranslate = translate.toLowerCase()));
    setTranslatedWords(translatedWords.concat(translate.toLowerCase()));
    if (translatedWords.length >= 10) {
      let deleteElement = translatedWords;
      deleteElement.shift();
      setTranslatedWords(deleteElement);
    }
    localStorage.setItem("translations", JSON.stringify(translatedWords));
  };

  const images = imageArray();

  const backToHome = () => {
    history.push("/");
  };

  const toProfile = () => {
    history.push("./Profile");
    localStorage.setItem("translations", JSON.stringify(translatedWords));
  };

  return (
    <AppContainer>
      <div>
        <div>
          <h1>Translation </h1>
          <button type="button" class="btn mr-3" onClick={toProfile}>
            Profile
          </button>
          <button type="button" class="btn mr-3" onClick={backToHome}>
            Home
          </button>
        </div>
        <div>
          <br></br>
          <input
            type="text"
            onChange={onTranslate}
            value={translate}
            placeholder="ENTER WORD TO TRANSLATE"
            className="form-control"
          />{" "}
          <br />
          <button className="btn btn-primary btn-lg" onClick={handleTranslate}>
            Translate
          </button>
        </div>

        <TranslationView wordToTranslate={wordToTranslate} />
      </div>
    </AppContainer>
  );
};

export default Translation;
