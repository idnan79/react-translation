import AppContainer from "../../hoc/AppContainer";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginAttemptAction } from "../../store/actions/loginActions";
import { Link, Redirect } from "react-router-dom";

const Login = () => {
  const dispatch = useDispatch();
  const { loginError, loginAttempting } = useSelector(
    (state) => state.loginReducer
  );
  const { loggedIn } = useSelector((state) => state.sessionReducer);

  const [credentials, setCredentials] = useState({
    username: "",
  });

  const onInputChange = (event) => {
    setCredentials({
      ...credentials,
      [event.target.id]: event.target.value,
    });
  };
  const onFormSubmit = (event) => {
    event.preventDefault(); //stop the page relaod,We use hook to dispatch
    dispatch(loginAttemptAction(credentials)); //login
  };

  return (
    <>
      {loggedIn && <Redirect to="/Translatepage" />}
      {!loggedIn && (
        <AppContainer>
          <form className="mt-3" onSubmit={onFormSubmit}>
            <h1>Login Lost in Translation</h1>

            <p>Get started! 👑</p>
            <img
              src={require("../image/Logo.png").default}
              height="200"
              width="200"
              alt=""
            />
            <div className="mb-3">
              <input
                id="username"
                type="text"
                placeholder=" What's your name"
                className="form-control"
                onChange={onInputChange}
              />

              <button type="submit" className="btn btn-dark btn-circle btn-xl">
                Login
              </button>
            </div>
          </form>
          {loginAttempting && <p>Trying to login..</p>}

          {loginError && (
            <div className="alert alert-danger" role="alert">
              <h4>Unsuccessful</h4>
              <p className="mb-0">{loginError}</p>
            </div>
          )}
          <p className="mb-3">
            <Link to="/Register">No account? register here</Link>
          </p>
        </AppContainer>
      )}
    </>
  );
};

export default Login;
