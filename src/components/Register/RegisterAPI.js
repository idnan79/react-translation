export const RegisterAPI = {
  register({ username }) {
    return fetch(" http://localhost:8000/users", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ username }),
    }).then(async (response) => {
      if (!response.ok) {
        const { error = "Unknown error occured" } = await response.json();
        throw new Error(error);
      }
      return response.json();
    });
  },
};
